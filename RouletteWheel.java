import java.util.Random;
public class RouletteWheel{
    private Random spin;
    private int numOfLastSpin=0;

    public RouletteWheel(){
        this.spin=new Random();
    }

    public void spin(){
        this.numOfLastSpin=this.spin.nextInt(37);
    }
    public int getValue(){
        return this.numOfLastSpin;
    }
}